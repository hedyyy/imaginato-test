//
//  PostLogin.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

struct PostLogin: ImaginatoRequest {
    typealias DataReponse = LoginResponse
    
    var user: User
    var parameters: Parameters? {
        let prms: [String: Any] = ["username": user.username,
                                   "password": user.password]
        return prms
    }
    
    var request: URLRequest!
    
    var path: String {
        return "/login"
    }

    var method: HTTPMethod {
        return .post
    }

    init(user: User) {
        self.user = user
    }
}
