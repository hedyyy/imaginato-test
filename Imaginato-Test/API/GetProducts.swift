//
//  GetProducts.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 13/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

struct GetProducts: ImaginatoRequest {
    typealias DataReponse = ProductResponse
    
    var parameters: Parameters? {
        return nil
    }
    
    var request: URLRequest!
    
    var path: String {
        return "/product/list"
    }
    
    var method: HTTPMethod {
        return .get
    }
}
