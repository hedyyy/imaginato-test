//
//  ProductCell.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 14/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(cellViewModel: ProductCellViewModel) {
        productName.text = cellViewModel.name
        productPrice.text = cellViewModel.price
        imgProduct.setImageFrom(urlString: cellViewModel.imageUrl)
    }
    
}
