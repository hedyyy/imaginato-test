//
//  LoginViewController.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    var viewModel: LoginViewModel!
    
    enum SegueIdentifier: String {
        case showProducts
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameField.becomeFirstResponder()
    }
    
    @IBAction func loginBtnTapped(_ sender: Any) {
        if let username = usernameField.text, let password = passwordField.text {
            if !username.isEmpty && !password.isEmpty {
                viewModel = LoginViewModel(username: username, password: password)
                authAction()
            } else {
                showBasicAlert(message: "Attribute Cannot Empty", completion: nil)
            }
        }
    }
    
    fileprivate func authAction() {
        showFullScreenLoading()
        viewModel.post { [weak self] (result) in
            guard let ws = self else { return }
            ws.dismissFullScreenLoading()
            if result == nil {
                ws.showBasicAlert(message: result?.errorMessage, completion: nil)
            } else {
                ws.performSegue(withIdentifier: "showProducts", sender: self)
            }
        }
    }
}

extension LoginViewController: SegueHandlerType {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(forSegue: segue) {
        case .showProducts:
            print("showProducts")
        }
    }
}
