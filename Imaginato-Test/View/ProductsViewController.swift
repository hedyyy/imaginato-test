//
//  ProductsViewController.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {
    
    var viewModel: ProductsViewModel = ProductsViewModel()
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier: String = "productCell"
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing data ...")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        fetchProduct()
    }
    
    fileprivate func setupTableview() {
        let nibname = UINib(nibName: "ProductCell", bundle: Bundle.main)
        tableView.register(nibname, forCellReuseIdentifier: cellIdentifier)
        tableView.addSubview(refreshControl)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    fileprivate func fetchProduct(withLoader: Bool = true) {
        withLoader ? showFullScreenLoading() : refreshControl.beginRefreshing()
        viewModel.fetch { [weak self] (result) in
            guard let ws = self else { return }
            withLoader ? ws.dismissFullScreenLoading() : ws.refreshControl.endRefreshing()
            if let err = result {
                ws.showBasicAlert(message: err, completion: nil)
            } else {
                ws.tableView.reloadData()
            }
        }
    }
    
    fileprivate func loadMoreProduct() {
        viewModel.loadMore { [weak self] (result) in
            guard let ws = self else { return }
            ws.tableView.reloadData()
        }
    }
    
    @objc func refresh() {
        fetchProduct(withLoader: false)
    }
}

extension ProductsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.totalSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalRowInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ProductCell
        let cellVM = viewModel.getProductCellVM(at: indexPath)
        cell.configure(cellViewModel: cellVM)
        return cell
    }
    
}

extension ProductsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.totalRowInSection - 1 {
            loadMoreProduct()
        }
    }
}
