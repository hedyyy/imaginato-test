//
//  UIImageView.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 14/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImageFrom(urlString: String) {
        let url = URL(string: urlString)
        self.kf.setImage(with: url)
    }
}
