//
//  LoadingManager.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import Foundation
import SVProgressHUD

protocol LoadingManager {}

extension LoadingManager where Self: UIViewController {
    func showFullScreenLoading() {
        SVProgressHUD.show()
        view.isUserInteractionEnabled = false
    }
    
    func dismissFullScreenLoading() {
        SVProgressHUD.dismiss()
        view.isUserInteractionEnabled = true
    }
}

extension UIViewController: LoadingManager {}
