//
//  AlertProtocol.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import Foundation
import UIKit

protocol AlertProtocol {}
extension AlertProtocol where Self: UIViewController {
    func showBasicAlert(title: String = "Warning", message: String? = nil, completion: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (_ ) in
            completion?()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showOKBasicAlert(title: String = "", message: String? = nil, onOK: (() -> ())? = nil, onCancel: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_ ) in
            onOK?()
        }
        let cancelAlert = UIAlertAction(title: "NO", style: .cancel) { (_ ) in
            onCancel?()
        }
        alert.addAction(cancelAlert)
        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }
    
    func showCustomBasicAlert(title: String = "", message: String? = nil, onOK: (() -> ())? = nil, onCancel: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_ ) in
            onOK?()
        }
        let cancelAlert = UIAlertAction(title: "Cancel", style: .cancel) { (_ ) in
            onCancel?()
        }
        alert.addAction(cancelAlert)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
}

extension UIViewController: AlertProtocol {}
