//
//  Product.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 13/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

struct Product: Decodable {
    let productId: String
    let productName: String
    let imageUrl: String
    let price: String
}

struct ProductResponse: Decodable {
    let status: String
    let errorMessage: String
    let products: [Product]
}
