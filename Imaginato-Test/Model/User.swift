//
//  User.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit
import RealmSwift

struct User: Encodable {
    var username: String
    var password: String
    
    init(user: String, pass: String) {
        self.username = user
        self.password = pass
    }
}

struct LoginResponse: Decodable {
    let errorCode: String
    let errorMessage: String
}

class Constant: Object {
    @objc dynamic var token = ""
}
