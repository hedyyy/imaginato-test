//
//  LoginViewModel.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 12/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

class LoginViewModel {
    
    fileprivate var login: PostLogin!
    fileprivate var user: User!
    
    init(username: String, password: String) {
        user = User(user: username, pass: password)
        login = PostLogin(user: user)
    }

    func post(completion: @escaping (LoginResponse?) -> ()) {
        login.request { (result) in
            if let res = result {
                completion(res)
            } else {
                completion(nil)
            }
        }
    }

}
