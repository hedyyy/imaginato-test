//
//  ProductCellViewModel.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 14/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

struct ProductCellViewModel {
    var name: String
    var imageUrl: String
    var price: String
    
    init(model: Product) {
        self.name = model.productName
        self.imageUrl = model.imageUrl
        self.price = model.price
    }
}
