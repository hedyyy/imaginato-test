//
//  ProductsViewModel.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 13/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit

class ProductsViewModel {
    fileprivate var getProducts: GetProducts!
    var model: [Product]!
    
    init() {
        model = [Product]()
        getProducts = GetProducts()
    }
    
    var totalSection: Int {
        return 1
    }
    
    var totalRowInSection: Int {
        return model.count
    }
    
    func fetch(completion: @escaping (String?) -> ()) {
        getProducts.request { [weak self] (result) in
            guard let ws = self else { return }
            if let res = result, res.errorMessage != "success" {
                completion(res.errorMessage)
            } else {
                ws.model = result?.products
                completion(nil)
            }
        }
    }
    
    func loadMore(completion: @escaping (String?) -> ()) {
        getProducts.request { [weak self] (result) in
            guard let ws = self else { return }
            if let res = result {
                ws.model = ws.model + res.products
            }
            completion(nil)
        }
    }
    
    func getProductCellVM(at indexPath: IndexPath) -> ProductCellViewModel {
        return ProductCellViewModel(model: model[indexPath.row])
    }
}
