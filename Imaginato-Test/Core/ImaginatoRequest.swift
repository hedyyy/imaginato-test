//
//  ImaginatoRequest.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 13/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit
import Foundation

protocol ImaginatoRequest {
    var request: URLRequest! { set get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    associatedtype DataReponse: Decodable
}

extension ImaginatoRequest {
    
    mutating func request(completion: @escaping (DataReponse?) -> ()) {
        
        let urlString: String = "\(BaseURL.getURL)\(path)"
        let url = URL(string: urlString)
        
        switch method {
        case .post:
            request = URLRequest(url: url!)
            if let params = parameters {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params)
                    request.httpBody = jsonData
                    print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
                } catch {
                    print("Error: unable to add parameters to POST request.")
                }
            }
        default:
            request = URLRequest(url: url!)
        }
        
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
       URLSession.shared.dataTask(with: request) { (data, response, err) in
            DispatchQueue.main.async {
                guard err == nil else {
                    completion(nil)
                    return
                }
                
                if let res = response as? HTTPURLResponse {
                    if let token = res.allHeaderFields["x-acc"] as? String {
                        let realm = ImaginatoRealm()
                        let constant = Constant()
                        constant.token = token
                        realm.saveToken(model: constant)
                    }
                }
                
                guard let data = data else {return}
                do {
                    let decoder =  JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let json =  try decoder.decode(DataReponse.self, from: data)
                    print(json)
                    completion(json)
                } catch let jsonErr {
                    print("Error serializing json", jsonErr)
                    completion(nil)
                }
            }
            }.resume()
    }
    
}
