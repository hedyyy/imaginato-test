//
//  ImaginatoRealm.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 14/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import UIKit
import RealmSwift

class ImaginatoRealm {
    var realm: Realm!
    
    func saveToken(model: Constant) {
        realm = try! Realm()
        if getToken() == nil {
            try! realm.write {
                realm.add(model)
            }
        }
    }
    
    func getToken() -> String? {
        realm = try! Realm()
        let token = realm.objects(Constant.self)
        if let tok = token.first {
            return tok.token
        }
        return nil
    }
}
