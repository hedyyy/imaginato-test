//
//  ImaginatoAPI.swift
//  Imaginato-Test
//
//  Created by Hedy Pamungkas on 13/06/18.
//  Copyright © 2018 Hedy Pamungkas. All rights reserved.
//

import Foundation

enum BaseURL: String {
    case prod = "https://private-222d3-homework5.apiary-mock.com/api"
    static var getURL: String {
        return prod.rawValue
    }
}

enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

typealias Parameters = [String: Any]
